<?php

namespace Nathanknz\SimpleSocketSelector;

use Nathanknz\SimpleSocketSelector\Exception\SelectorException;

class Selector
{
    protected $sockets = [];

    public function __construct()
    {
        $this->sockets = [
            'read' => null,
            'write' => null,
            'except' => null,
        ];
    }

    public function select($timeout)
    {
        $read = $this->sockets['read'];
        $write = $this->sockets['write'];
        $except = $this->sockets['except'];
        $tv_sec = null;
        $tv_usec = 0;

        if (is_numeric($timeout)) {
            $tv_sec = floor($timeout);
            $tv_usec = ($timeout - $tv_sec) * 1000000;
        }

        $num_changed_sockets = socket_select($read, $write, $except, $tv_sec, $tv_usec);

        if ($num_changed_sockets === false) {
            throw new SelectorException(socket_strerror(socket_last_error()), socket_last_error());
        } elseif ($num_changed_sockets > 0) {
            $changed_sockets = [
                'read' => [],
                'write' => [],
                'except' => [],
            ];

            if (is_array($this->sockets['read'])) {
                foreach ($this->sockets['read'] as $name => $socket) {
                    if (in_array($socket, $read)) {
                        $changed_sockets['read'][] = $name;
                    }
                }
            }
            if (is_array($this->sockets['write'])) {
                foreach ($this->sockets['write'] as $name => $socket) {
                    if (in_array($socket, $write)) {
                        $changed_sockets['write'][] = $name;
                    }
                }
            }
            if (is_array($this->sockets['except'])) {
                foreach ($this->sockets['except'] as $name => $socket) {
                    if (in_array($socket, $except)) {
                        $changed_sockets['except'][] = $name;
                    }
                }
            }

            return $changed_sockets;
        }

        return false;
    }

    public function registerSocket($type, $name, $socket)
    {
        if (array_key_exists($type, $this->sockets)) {
            if (!is_array($this->sockets[$type])) {
                $this->sockets[$type] = [];
            }
            $this->sockets[$type][$name] = $socket;
        }

        return $this;
    }

    public function unregisterSocket($type, $name)
    {
        if (array_key_exists($type, $this->sockets) && is_array($this->sockets[$type])) {
            if (isset($this->sockets[$type][$name])) {
                unset($this->sockets[$type][$name]);
            }
            if (!count($this->sockets[$type])) {
                $this->sockets[$type] = null;
            }
        }

        return $this;
    }

    public function registerStream($type, $name, $stream)
    {
        $socket = socket_import_stream($stream);

        return $this->registerSocket($type, $name, $socket);
    }

    public function unregisterStream($type, $name)
    {
        return $this->unregisterSocket($type, $name);
    }

    public function registerReadSocket($name, $socket)
    {
        return $this->registerSocket('read', $name, $socket);
    }

    public function unregisterReadSocket($name)
    {
        return $this->unregisterSocket('read', $name);
    }

    public function registerReadStream($name, $socket)
    {
        return $this->registerStream('read', $name, $socket);
    }

    public function unregisterReadStream($name)
    {
        return $this->unregisterStream('read', $name);
    }

    public function registerWriteSocket($name, $socket)
    {
        return $this->registerSocket('write', $name, $socket);
    }

    public function unregisterWriteSocket($name)
    {
        return $this->unregisterSocket('write', $name);
    }

    public function registerWriteStream($name, $socket)
    {
        return $this->registerStream('write', $name, $socket);
    }

    public function unregisterWriteStream($name)
    {
        return $this->unregisterStream('write', $name);
    }

    public function registerExceptSocket($name, $socket)
    {
        return $this->registerSocket('except', $name, $socket);
    }

    public function unregisterExceptSocket($name)
    {
        return $this->unregisterSocket('except', $name);
    }

    public function registerExceptStream($name, $socket)
    {
        return $this->registerStream('except', $name, $socket);
    }

    public function unregisterExceptStream($name)
    {
        return $this->unregisterStream('except', $name);
    }
}
